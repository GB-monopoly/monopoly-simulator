"""
Board object for handling
"""
# Std imports
from typing import TYPE_CHECKING, Dict, List
import logging

# External imports
# n/a

# Internal Imports
from . import game
from . import board_spaces
from . import players

if TYPE_CHECKING:
    from .board_spaces import BaseSpace

# Constants/Variables
logger = logging.getLogger(__name__)


class Board:
    def __init__(self, parent_game: "game.Game") -> None:
        self._game = parent_game
        board_list = self._load_board()
        self._board_spaces: Dict[str, BaseSpace] = {
            prop.static_hash: prop for prop in board_list
        }

    def move_player_num_spaces(self, player: "players.BasePlayer", num_spaces: int):
        raise NotImplementedError

    def move_player_to_location(
        self, player: "players.BasePlayer", target_location: "board_spaces.BaseSpace"
    ):
        raise NotImplementedError

    def get_player(self, static_hash: str) -> "players.BasePlayer":
        return self._game.get_player(static_hash)

    def _load_board(self) -> List["board_spaces.BaseSpace"]:
        """Function to load board from example boards

        Loads the uk standard board
        """
        raise NotImplementedError

    def __getitem__(self, index) -> board_spaces.BaseSpace:
        return self._board_spaces[index]

    def __len__(self):
        return len(self._board_space_list)
