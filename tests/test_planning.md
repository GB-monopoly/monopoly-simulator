## Test Planning

Just some notes on planning tests



# System tests

A set test with the default settings that should always end in the same way, e.g. 3 specific players

Memory tests:
    Long running tests with lots of players but rents etc are all very low. 
    Start and stop lots of games very quickly (could even try stopping garbage collecting)
    
    
Game are repeatable (e.g. with the same random seed will have the same result)    
    
# Integration tests
