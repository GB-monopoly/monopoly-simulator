from setuptools import setup, find_packages

setup(
    name="monopoly",
    version="0.0.1",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    url="",
    license="",
    author="George Barnett",
    author_email="",
    description="",
)
