# Std imports
from typing import Any
import logging
import hashlib
import json

# External imports
# n/a

# Internal Imports
# n/a

# Constants/Variables
logger = logging.getLogger(__name__)


def create_object_hash(obj: Any, hash_method: str = "md5") -> str:
    # Convert to string to hash
    # Use json with sort_keys to ensure repeated consistency with key ordering
    obj_str = json.dumps(obj, sort_keys=True)
    if hash_method == "md5":
        hash_str = hashlib.md5(obj_str.encode("utf-8")).hexdigest()
    else:
        ValueError(f"Invalid hash_method {hash_method}. Options include: ['md5']")
    logger.debug(f"Hashed {obj_str} to {hash_str}")
    return hash_str
