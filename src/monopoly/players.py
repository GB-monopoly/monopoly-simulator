# Std imports
import logging
import abc
import weakref
from typing import TYPE_CHECKING, cast, Optional, List, Dict, Any

# External imports
# n/a

# Internal Imports
from .common import create_object_hash
from . import game
from . import board_spaces

if TYPE_CHECKING:
    from .board_spaces import Property
    from .game import Game

# Constants/Variables
# logging config during testing is set in setup.cfg
logger = logging.getLogger(__name__)


class BasePlayer(abc.ABC):
    def __init__(self, name: str, game: "game.Game" = None):
        # Static variables, gettable but not settable (with public api)
        self._name: str = name

        # Player State, attributes that change throughout the game
        self.money: int = 0
        self.board_position: int = 0
        self._properties: weakref.WeakSet = weakref.WeakSet()
        self._game: Optional["Game"] = None  # type: ignore

    def join_game(self, game: "game.Game"):
        self._game = game
        self.receive_money(game.settings["initial_money"])

    @property
    def static_attributes(self) -> Dict[str, Any]:
        """static properties/attributes

        Get the static and immutable objects that are a created at
        initialisation, e.g. name, style of play. Returns a dict of the
        attributes and their value. The key should be the public name of the
        object. Base static attributes are type and name.

        Note:The type will return the type of the child class,
        not the type of the base object"""
        return {"type": type(self).__name__, "name": self.name}

    @property
    def static_hash(self) -> str:
        """hash of static attributes

        make a identifying hash from the static attributes"""

        # TODO cache the hash as it shouldn't change
        static_hash = create_object_hash(self.static_attributes)
        logger.debug(f"Calculated {self.name} hash to be {static_hash}")
        return static_hash

    # Static attribute getters
    @property
    def name(self) -> str:
        """Name of the player"""
        return self._name

    @property
    def properties(self) -> List["Property"]:
        """The properties owned by the player"""
        if self._game is None:
            raise ValueError(
                "Player has not _game attribute. Players must be joined to a game."
            )
        # All owned properties should be a Property not BaseSpace
        owned_properties: List[Property] = [
            # Type checking casting
            cast(board_spaces.Property, self._game.board[prop_hash])
            for prop_hash in self._properties
        ]
        # Run time type checking
        assert all(
            [isinstance(prop, board_spaces.Property) for prop in owned_properties]
        )
        logger.debug(
            f"Player {self.name} owns {[prop.name for prop in owned_properties]}"
        )
        return owned_properties

    def add_property(self, property: "Property") -> None:
        """Add property to the owned properties

        :param property: The property to add
        """
        self._properties.add(property.static_hash)

    def remove_property(self, property: "Property") -> None:
        """Remove property from owned properties

        :param property: The property to remove
        """
        self._properties.remove(property.static_hash)

    @property
    @abc.abstractmethod
    def money_available(self) -> int:
        """The total liquid money availble

        This includes strategies for increasing money instantly, e.g. mortgaging
        properties or engaging in trades. Different players will have different
        strategies for raising money and so this is an abstract method.

        :return: the total money available to pay
        """
        pass

    @abc.abstractmethod
    def will_buy(self, property: "Property") -> bool:
        """Whether a property should be bought

        If there is an available property that the player can buy, will the player buy
        it. This means the property must not have an owner or the owner must be willing
        to trade it.

        :param property: the property that is available
        """
        pass

    def pay_bank(self, amount: int) -> None:
        """Pay the bank

        :param amount: the amount the player will pay the bank players.
        """
        logger.info(f"Player{self.name} payed the bank {amount}")
        self._reduce_money(amount)

    def pay_player(self, amount: int, recipient_player: "BasePlayer") -> None:
        """Pay another player some amount from this player"""
        logger.info(f"Player{self.name} payed {recipient_player.name} {amount}")
        recipient_player.receive_money(amount)
        self._reduce_money(amount)

    def receive_money(self, amount: int):
        """Recieve money from the bank or from another player

        :param amount: amount to increase the money of the player by
        """
        logger.debug(
            f"Player {self.name} increase money from {self.money} to {self.money+amount}"
        )
        self.money += amount

    @abc.abstractmethod
    def _reduce_money(self, amount: int) -> None:
        """Reduce player money

        If the player goes below 0 money different player types can raise money through
         or trades. In this function start the proceedings for bankruptency.

        :param amount: the amount to the available money must decrease by.
        """
        pass


class SimplePlayer(BasePlayer):
    def __init__(self, name: str):
        """A simple player who only plays with cash

        SimplePlayer only plays with the cash available, does not mortgage, but will buy
        a property if the cash is available.
        """
        logger.debug("Creating Simple Player: {name}")
        super(SimplePlayer, self).__init__(name)

    @property
    def static_attributes(self) -> Dict[str, Any]:
        return super(SimplePlayer, self).static_attributes

    @property
    def money_available(self) -> int:
        return self.money

    def will_buy(self, property: "Property") -> bool:
        return property.value <= self.money_available

    def _reduce_money(self, amount: int) -> None:
        if self.money_available < amount:
            # self.go_bankrupt()
            raise NotImplementedError
        else:
            self.money -= amount
