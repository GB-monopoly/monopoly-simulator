# Std imports
from unittest import mock

# External imports
import pytest

# Internal Imports
from monopoly.game import Game, TurnGenerator

# Constants/Variables
# n/a


@pytest.fixture(params=[["Anna", "Bob"], ["Anna", "Bob", "Charles", "Dan"]])
def example_players(request):
    player_names = request.param
    players = []
    for p_name in player_names:
        p_mock = mock.MagicMock()
        p_mock.name = p_name
        players.append(p_mock)
    return player_names.copy(), players.copy()


class TestGame:
    @pytest.fixture(scope="function")
    def mocked_game(self) -> Game:
        """Patch the __init__ function to create a mock like object

        Patching the init allows us to create an 'empty' Game object. The lambda function
        patches the __init__ method (which would normally take self and the normal
        parameters) and returns None. Then the attributes of the instantiated Game can be
        edited like a Mock object. This has three advantages:
        1). We don't run init method fully, which may be complex and require instantiating
        things like the board.
        2) We have to actively set the desired state.
        3) All of the methods are still availble (although they may not get type hinted in
        IDEs)

        :return: Instantiated Game object with no attributes
        """
        with mock.patch.object(
            Game, "__init__", lambda self_var, players, settings: None, None
        ):
            yield Game(None, None)

    def test_game_players(self, mocked_game, example_players):
        player_names, player_list = example_players
        mocked_game._players_in_game = {}

        for player in player_list:
            mocked_game.add_player(player)

        assert mocked_game.players_in_game == player_list

    def test_remove_player(self, mocked_game, example_players):
        player_names, player_list = example_players
        mocked_game._players_in_game = {
            player.static_hash: player for player in player_list
        }
        mocked_game._turn_generator = mock.MagicMock()
        mocked_game._turn_generator.remove_player = mock.Mock()

        player_to_remove = player_list[1]
        mocked_game.remove_player(player_to_remove)
        assert player_to_remove not in mocked_game._players_in_game
        mocked_game._turn_generator.remove_player.assert_called_once()

    class TestTurn:

        EXAMPLE_ROLLS = ([1], [2, 2], [1, 6], [6, 6, 6])
        ROLL_AGAIN_BOOL = (False, True, False, True)

        @pytest.fixture(params=EXAMPLE_ROLLS)
        def example_roll(self, request):
            return request.param

        @pytest.fixture(params=zip(EXAMPLE_ROLLS, ROLL_AGAIN_BOOL))
        def roll_again_tuple(self, request):
            """Gives tuple of roll and whether to have another turn based off that roll"""
            return request.param

        def test_roll_dice(self, mocked_game, example_roll):
            with mock.patch("random.randint", side_effect=example_roll):
                assert mocked_game._roll_dice(len(example_roll)) == example_roll

        def test_check_another_turn(self, mocked_game, roll_again_tuple) -> bool:
            roll, roll_again = roll_again_tuple
            assert mocked_game._check_another_turn(roll) == roll_again

        @pytest.mark.parametrize(
            "sets_of_rolls",
            [([1, 2],), ([2, 2], [1, 4]), ([6, 6], [5, 5], [6, 5])],
        )
        def test_multiple_rolls(self, mocked_game, sets_of_rolls):
            # Note this erquires check_another_turn to work
            mocked_game.settings = {"num_dice": 2}
            mocked_game._execute_roll = mock.MagicMock()
            mocked_game._roll_dice = mock.MagicMock(side_effect=sets_of_rolls)

            mocked_player = mock.MagicMock()

            mocked_game._turn(mocked_player)

            # _execute_roll should have been executed as many times as we have rolls.
            # It will raise a StopIteration if it is executed more times # than it has
            # side effets to return
            mocked_game._execute_roll.call_count = len(sets_of_rolls)


class TestTurnGenerator:
    def test_turn_generator(self, example_players):
        player_names, player_list = example_players
        turn_generator = TurnGenerator(player_list)

        # Go round the set of names 3 times
        expected_names = 3 * player_names
        for output_player, expected_name in zip(turn_generator, expected_names):
            assert output_player.name == expected_name

    @pytest.mark.parametrize("index_to_remove", [0, 1, -1])
    def test_player_removed(self, index_to_remove, example_players):
        player_names, player_list = example_players
        turn_generator = TurnGenerator(player_list)

        # Remove player from initial list
        removed_player = player_list.pop(index_to_remove)
        player_names.pop(index_to_remove)
        turn_generator.remove_player(removed_player)
        for output_player, expected_name in zip(turn_generator, player_names):
            assert output_player.name == expected_name
