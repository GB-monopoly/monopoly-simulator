# Std imports #
import logging

# External imports #
import pytest

# Internal Imports #
from monopoly.common import create_object_hash

# Constants/Variables #
# logging config during testing is set in setup.cfg
logger = logging.getLogger(__name__)


class TestHashing:
    @pytest.mark.parametrize(
        "obj_to_hash",
        [
            ("Yes", "Yes"),
            # ({1, 2, 3}, {3, 2, 1}),
            (
                {"key1": 1, "key2": "text2", "key3": [1, 2, 3]},
                {"key3": [1, 2, 3], "key1": 1, "key2": "text2"},
            ),
        ],
    )
    def test_same_str(self, obj_to_hash):
        output_hashes = {create_object_hash(obj) for obj in obj_to_hash}

        # check all output hashes are the same
        assert len(set(output_hashes)) == 1

    def test_nested_dicts(self):
        dict1 = {
            "key1": 1,
            "key2": {"key1": "nested1", "key2": "nested2", "key3": "nested3"},
        }
        hash1 = create_object_hash(dict1)
        logger.debug(f"Hash 1: {hash1}")
        dict2 = {
            "key2": {"key3": "nested3", "key1": "nested1", "key2": "nested2"},
            "key1": 1,
        }
        hash2 = create_object_hash(dict2)
        logger.debug(f"Hash 2: {hash2}")
        assert hash1 == hash2

    @pytest.mark.parametrize(
        "obj_to_hash",
        [
            # Caps are not equivalent strings
            ("Yes", "yes"),
            # Sub sets are invalid
            # ({1, 2, 3}, {3, 3, 1}),
            # Lists should maintain order
            ([1, 2, 3], [1, 2, 3]),
            # Sub dictionaries are invalid
            (
                {"key1": 1, "key2": "text2"},
                {"key1": 1},
            ),
        ],
    )
    def test_different_strings(self, obj_to_hash):
        output_hashes = {create_object_hash(obj) for obj in obj_to_hash}

        # check all output hashes are the different
        assert len(set(output_hashes)) == len(output_hashes)
