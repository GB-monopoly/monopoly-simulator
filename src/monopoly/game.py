# Std imports
from typing import TYPE_CHECKING, Dict, List, Any, Generator, Iterable
import logging
import random

# External imports
# n/a

# Internal Imports
from . import board
from . import players

if TYPE_CHECKING:
    from .players import BasePlayer

# Constants/Variables
logger = logging.getLogger(__name__)
DEFAULT_SETTINGS = {
    "max_turns": 1000,
    "num_dice": 2,
    "number_repeat_rolls": 3,
    "initial_money": 1500,
}


class TurnGenerator(Generator):
    def __init__(self, player_list: List["players.BasePlayer"]):
        """Generator for turn ordering

        This handles whose turn is next. If players are removed from the game they can be
        removed and maintain ordering for other players
        """
        # Copy list internal and external order changes don't mess with others lists
        self._players = player_list.copy()

    def send(self, ignored_arg):
        player_turn = self._players[0]
        logger.debug(f"Player turn: {self._players[0].name}")
        self._players.append(self._players.pop(0))
        return player_turn

    def throw(self, type=None, value=None, traceback=None):
        raise StopIteration

    def remove_player(self, player: "players.BasePlayer"):
        logger.debug(f"Removed {player.name} from turn generator")
        self._players.remove(player)


class Game:
    def __init__(
        self, players: List["players.BasePlayer"], settings: Dict[str, Any] = {}
    ):
        self.board = board.Board(parent_game=self)
        self._comunity_chest: List[Any] = []
        self._chance_cards: List[Any] = []
        self.rounds = 0

        # Use default settings then overload
        self.settings = DEFAULT_SETTINGS
        for key, value in settings.items():
            self.settings[key] = value

        self._players_in_game = {
            player.static_hash: player for player in players.copy()
        }
        # Use protected players object so we can remove players from turn generator
        self.starting_players: List["BasePlayer"] = []
        for player in self.starting_players:
            player.join_game(self)
        self._turn_generator = TurnGenerator(self.starting_players)

    @property
    def players_in_game(self) -> List["players.BasePlayer"]:
        return list(self._players_in_game.values())

    def get_player(self, static_hash: str) -> players.BasePlayer:
        return self._players_in_game[static_hash]

    def add_player(self, player: "players.BasePlayer") -> None:
        assert player not in self.players_in_game
        self._players_in_game[player.static_hash] = player

    def remove_player(self, player) -> None:
        logger.info(f"Removing {player.name} from game :(.")
        num_players = len(self.players_in_game)
        self._turn_generator.remove_player(player)
        # TODO: Remove player from board spaces
        del self._players_in_game[player.static_hash]
        assert len(self.players_in_game) == num_players - 1

    def play(self):
        """Play game of monopoly

        Loop through players (that are still in the game)    and execute their turn.
        """
        logger.info("Starting game...")
        turn_counter = 0
        while turn_counter <= self.settings["max_turns"]:
            to_play: players.BasePlayer = next(self._turn_generator)
            self._turn(to_play)
            turn_counter += 1

    def _roll_dice(self, num_dice) -> List[int]:
        """ Roll a set of d6 dice"""
        rolls = [random.randint(1, 6) for i in num_dice * [0]]
        logger.info(f"Dice roller: {rolls}")
        return rolls

    def _turn(self, player: "players.BasePlayer") -> None:
        """Take a players turn"""
        logger.debug(f"Starting turn for {player.name}")
        rolls: List[List[int]] = []
        while not rolls or self._check_another_turn(rolls[-1]):
            rolls.append(self._roll_dice(self.settings["num_dice"]))
            self._execute_roll(player, rolls[-1])
        logger.debug(f"Finishing {player.name} turn")

    def _execute_roll(self, player: "players.BasePlayer", roll: Iterable[int]) -> None:
        """Execute the effects of the movement of a given roll

        Move the player and execute the given effects of that movement. This includes
        land effects etc.
        """
        logger.debug(f"Executing roll: {roll}")

        # Move player by roll amount
        self.board.move_player_num_spaces(player, sum(roll))
        # Work out trades etc
        pass

    def _check_another_turn(self, roll: List[int]) -> bool:
        """Check if a single set of dice rolls gets another turn

        Current implementation is if all rolls are the same, unless it is a single dice
        which never gets another turn.
        """
        if len(roll) > 1:
            result = all(die == roll[0] for die in roll)
        else:
            result = False
        logger.info(f"Roll {roll} another turn: {result}")
        return result
