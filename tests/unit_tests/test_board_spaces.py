# Std imports
from unittest import mock

# External imports
import pytest

# Internal Imports
from monopoly.board_spaces import Property
from monopoly.board import Board

# Constants/Variables
# n/a


class TestProperty:
    @pytest.fixture
    def player_1(self) -> mock.MagicMock:
        return self.create_player("player_1")

    @pytest.fixture
    def player_2(self) -> mock.MagicMock:
        return self.create_player("player_2")

    @pytest.fixture(scope="function")
    def mocked_board(self, player_1, player_2) -> Board:
        """Patch the __init__ function to create a mock like object

        Patching the init allows us to create an 'empty' Board object. The lambda function
        patches the __init__ method (which would normally take self and the normal
        parameters) and returns None. Then the attributes of the instantiated Game can be
        edited like a Mock object. This has three advantages:
        1). We don't run init method fully, which may be complex and require instantiating
        things like the board.
        2) We have to actively set the desired state.
        3) All of the methods are still availble (although they may not get type hinted in
        IDEs)

        :return: Instantiated board object with no attributes
        """
        with mock.patch.object(Board, "__init__", lambda self_var, parent_game: None):
            board = Board(None)
            player_lookup = {
                player.static_hash: player for player in [player_1, player_2]
            }

            def mock_get_player(static_hash):
                return player_lookup[static_hash]

            board.get_player = mock_get_player
            yield board

    def create_player(self, name) -> mock.MagicMock:
        player = mock.MagicMock()
        player.name = name
        player.money = 1
        player.money_available = 1
        return player

    @pytest.fixture
    def example_property(self, mocked_board) -> Property:
        return Property(
            parent_board=mocked_board, name="test_property", value=1, rent=1
        )

    @pytest.mark.parametrize(
        "test_owner",
        [
            pytest.lazy_fixture("player_1"),
            None,
        ],
    )
    def test_set_owner(self, example_property, test_owner):
        assert example_property.owner is None
        example_property.set_owner(test_owner)
        assert example_property.owner == test_owner

    def test_buy(self, example_property, player_1, player_2):
        example_property.set_owner = mock.MagicMock()
        example_property.pay_bank = mock.MagicMock()
        example_property.buy(player_1)
        example_property.set_owner.assert_called_with(player_1)
        player_1.pay_bank.assert_called_with(example_property.value)

    def test_pay_rent(self, example_property, player_1, player_2):
        player_2.pay_player = mock.MagicMock()
        example_property._owner_hash = player_1.static_hash
        example_property.pay_rent(player_2)
        player_2.pay_player.assert_called_with(
            amount=example_property.rent, recipient_player=player_1
        )

    class TestLandEffect:
        def test_unowned(self, player_1, example_property):
            assert example_property.owner is None
            player_1.will_buy = mock.MagicMock(return_value=True)
            example_property.buy = mock.MagicMock()
            example_property.pay_rent = mock.MagicMock()

            example_property.land_effect(player_1)

            example_property.buy.assert_called_once()
            example_property.pay_rent.assert_not_called()

        def test_owned_same_player(self, example_property: Property, player_1):
            example_property.set_owner(player_1)
            example_property.pay_rent = mock.MagicMock()
            example_property.buy = mock.MagicMock()

            example_property.land_effect(player_1)

            example_property.pay_rent.assert_not_called()
            example_property.buy.assert_not_called()

        def test_owned_different_player(
            self, example_property: Property, player_1, player_2
        ):
            example_property.set_owner(player_1)
            example_property.pay_rent = mock.MagicMock()

            # example_property.land_effect(player_2)
            example_property.pay_rent(player=player_2)

            # Check only called once
            example_property.pay_rent.assert_called_once()
            # Check called as arg or kwarg
            # (player_2) or (player=player_2)
            assert example_property.pay_rent.call_args[0] == (
                player_2,
            ) or example_property.pay_rent.call_args[1] == {"player": player_2}
            example_property.pay_rent.assert_called_with(player=player_2)
