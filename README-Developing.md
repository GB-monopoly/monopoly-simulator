# Development Notes

### Linting

Use black and flake8 to ensure linting and consistent code formatting. 

### CI (planned)

There will be 4 stages of CI. Stages 1&2 are run on every commit and must pass before merging with the development branch. Stages 3&4 must pass before releases.

1. Linting - Black and flake8 must return no errors
2. Unit/Integration tests - Must pass and have decent code coverage
3. System Tests - These are long running tests
4. Package Requirements - Check the default, minimum and latest versions of all python packages using tox.

### Doc Strings

Use a slight variation of reST style as shown below:
```
"""summary goes here

This is a reST style. Here goes the longer explanations and reasoning, though some functions do not require this longer
section.

:param param1: this is a first param
:param param2: this is a second param
:returns: this is a description of what is returned
:raises keyError: raises an exception
"""
```

### File Headers/Imports

Import packages as shown below:
```
# Std imports
import os

# External imports
# n/a

# Internal Imports
from .game import Game

# Constants/Variables
logger = logging.getLogger(__name__)

# Python code start here
```


### Object Hiearachy

This diagram shows a rough object hierachy for the various classes. Parent classes may import and strongly reference
their child classes, but child classes should use Forward Referencing and weak references to their parents. This ensure
no memory leaks and correct object ownership.

```
Object Hierachys
    - Game
        - Board
            - Board Spaces
            - example boards                      
        - Player*
        - Settings
            - Free parking
            - Go amount
            - Auctions



 *Generally Board acts on Player but strictly they are the same level.
```

