# Std imports
from abc import ABC
import logging

# External Imports
from typing import TYPE_CHECKING, Optional, Dict, Any, List

# Internal imports
from . import board
from . import common
from . import players

if TYPE_CHECKING:
    from .players import BasePlayer

    # from .board import Board

# Constants/Variables
logger = logging.getLogger(__name__)


class BaseSpace(ABC):
    def __init__(self, parent_board: "board.Board", name: str):
        self._name = name
        self._board: "board.Board" = parent_board

    @property
    def name(self) -> str:
        return self._name

    @property
    def board(self):
        return self._board

    def land_effect(self, player: "BasePlayer") -> None:
        """Effect when player lands on space"""
        pass

    @property
    def static_attributes(self) -> Dict[str, Any]:
        """static properties/attributes

        Get the static and immutable objects that are a created at
        initialisation, e.g. name and rent. Returns a dict of the
        attributes and their value. The key should be the public name of the
        object."""
        return {"name": self.name}

    @property
    def static_hash(self) -> str:
        """hash of static attributes

        make a identifying hash from the static attributes"""

        # TODO cache the hash as it shouldn't change
        static_hash = common.create_object_hash(self.static_attributes)
        logger.debug(f"Calculated {self.name} hash to be {static_hash}")
        return static_hash


class EmptySpace(BaseSpace):
    def __init__(self, parent_board: "board.Board", name: str):
        super().__init__(parent_board, name)


class JailSpace(BaseSpace):
    def __init___(self, parent_board: "board.Board", name: str):
        super().__init__(parent_board, name)
        self._players_in_jail: List[BasePlayer] = []

    def land_effect(self, player: "BasePlayer", visiting: bool = True) -> None:
        """Effect when player lands on space

        :param player: the landing player"""
        raise NotImplementedError


class Property(BaseSpace, ABC):
    def __init__(
        self, parent_board: "board.Board", name: str, value: int, rent: int = 0
    ):
        super().__init__(parent_board, name)
        # static attributes
        self._value: int = value
        self._rent: int = rent

        # mutable attributes
        self._owner_hash: Optional[str] = None
        self.mortgaged: bool = False

    @property
    def static_attributes(self) -> Dict[str, Any]:
        attributes = super().static_attributes
        attributes["value"] = self.value
        attributes["rent"] = self.rent
        return attributes

    @property
    def value(self) -> int:
        return self._value

    @property
    def rent(self) -> int:
        return self._rent

    @property
    def owner(self) -> Optional["BasePlayer"]:
        if self._owner_hash is None:
            return None
        owner_player = self._board.get_player(static_hash=self._owner_hash)
        if owner_player is None and self._owner_hash is not None:
            logger.debug(
                f"Setting owner hash to None as no player was found with hash "
                f"{self._owner_hash}"
            )
            self.set_owner(player=None)
        return owner_player

    def set_owner(self, player: Optional["players.BasePlayer"]) -> None:
        logger.debug(f"Setting {self.name} owner to {player}")
        self._owner_hash = player.static_hash if player is not None else None

    @property
    def mortgage_value(self) -> int:
        return int(self.value / 2)

    def mortgage_property(self) -> None:
        raise NotImplementedError

    def land_effect(self, player: "BasePlayer") -> None:
        """Effect when player lands on space

        For a property, this means the landing player must pay rent or has the option
        to buy.

        :param player: the landing player
        """
        logger.info(f"{player.name} landed on {self.name}.")
        logger.debug(f"Current owner of {self.name} is: {self.owner}")
        if self.owner is None:
            if player.will_buy(self):
                self.buy(player)
            else:
                # Auction property
                raise NotImplementedError
        elif self.owner != player:
            self.pay_rent(player)

    def buy(self, player: "BasePlayer"):
        """Buy the property

        :param player: the player who is buying the property
        """
        assert player is not None
        logger.debug(f"{player.name} is buying {self.name}.")
        assert player.money_available >= self.value
        player.pay_bank(self.value)
        self.set_owner(player)
        logger.info(f"{player.name} bought {self.name}.")

    def pay_rent(self, player: "BasePlayer"):
        """Pay the owner of the property rent

        :param player: the player must apy rent
        """
        assert player is not None
        assert self.owner is not None
        logger.info(f"{player.name} must pay {self.owner.name} {self.rent} in rent.")
        player.pay_player(amount=self.rent, recipient_player=self.owner)


class StreetProperty(Property):
    def __init__(self, parent_board: "board.Board", name: str, value: int):
        super().__init__(parent_board, name, value)
        self.houses = 0
