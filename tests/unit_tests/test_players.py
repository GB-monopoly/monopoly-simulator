# Std imports
from unittest import mock
import logging
from typing import Dict, Any

# External imports
import pytest

# Internal Imports
from monopoly.players import BasePlayer
from monopoly.board_spaces import Property

# Constants/Variables
# logging config during testing is set in setup.cfg
logger = logging.getLogger(__name__)


class NonAbstractTestPlayer(BasePlayer):
    def __init__(self, name: str = None):
        super().__init__(name)
        self._game = mock.MagicMock()

    @property
    def static_attributes(self) -> Dict[str, Any]:
        return super().static_attributes

    def money_available(self) -> int:
        return self.money

    def will_buy(self, property: "Property") -> bool:
        return True

    def _reduce_money(self, amount: int) -> None:
        pass


class TestPlayer:
    @pytest.fixture(scope="function")
    def example_player_1(self) -> BasePlayer:
        yield NonAbstractTestPlayer("Test Player 1")

    @pytest.fixture(scope="function")
    def example_player_2(self) -> BasePlayer:
        yield NonAbstractTestPlayer("Test Player 2")

    class TestAttributes:
        def test_base_static_attributes(self, example_player_1):

            required_attributes = {
                "type": type(example_player_1).__name__,
                "name": example_player_1.name,
            }
            logger.debug(f"Player required Attritbutes: {required_attributes}")

            # Get the base player attributes
            base_attributes = super(
                type(example_player_1), example_player_1
            ).static_attributes

            # required attritubes must be subset of player 1 static attributes
            assert set(required_attributes.items()).issubset(
                set(base_attributes.items())
            )

            # check that static attributes contain child type not parent type
            assert (
                example_player_1.static_attributes["type"]
                == type(example_player_1).__name__
            )

    class TestPaying:
        def test_pay_bank(self, example_player_1):
            example_player_1._reduce_money = mock.MagicMock()
            example_player_1.pay_bank(100)
            example_player_1._reduce_money.assert_called_once_with(100)

        def test_pay_player(self, example_player_1, example_player_2):
            example_player_1._reduce_money = mock.MagicMock()
            example_player_2.receive_money = mock.MagicMock()
            example_player_1.pay_player(100, example_player_2)
            example_player_1._reduce_money.assert_called_once_with(100)
            example_player_2.receive_money.assert_called_once_with(100)

        def test_receive_money(self, example_player_1):
            example_player_1.money = 0
            example_player_1.receive_money(100)
            assert example_player_1.money == 100

    class TestProperties:
        def mock_property(self, name) -> Property:
            property = mock.MagicMock()
            property.name = name
            property.static_hash = name + "_hash"
            return property

        @pytest.fixture(
            params=[["prop_1"], ["prop_1", "prop_2"], ["prop_1", "prop_2", "prop_3"]]
        )
        def example_properties(self, request):
            property_name_list = request.param
            yield [self.mock_property(name) for name in property_name_list]

        def test_remove_property(self, example_player_1, example_properties):
            example_player_1._properties = [
                prop.static_hash for prop in example_properties
            ]
            to_remove = example_properties.pop(0)
            example_player_1.remove_property(to_remove)
            assert example_player_1._properties == [
                prop.static_hash for prop in example_properties
            ]
